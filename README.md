# Description

This module allows email notifications upon advancedqueue events.
E.g. when queue processing fails for an item after 5 retries, send an email to
website administrators to notify them.
Email sending is done via the Drupal Symfony Mailer module.

# Dependencies

* [Advanced Queue](https://www.drupal.org/project/advancedqueue)
  * Requires patch https://www.drupal.org/project/advancedqueue/issues/3255444
    to be applied!
* [Drupal Symfony Mailer](https://www.drupal.org/project/symfony_mailer)

# Configuration

* Install and enable this module
* Set up a queue through advancedqueue and add logic to provide it with jobs
* Configure Symfony Mailer by adding a new Policy of type 'Advanced Queue mail'
* Select the appropriate subtype to receive email notifications for (usually
  'Job failure'). Note: you will have to add a policy for the subtypes you are
  not interested in as well - add element "Skip sending" for those.
* Further configure the policy by setting email subject, body, recipient, ...
  as desired.
