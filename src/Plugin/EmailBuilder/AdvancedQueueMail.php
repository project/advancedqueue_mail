<?php

namespace Drupal\advancedqueue_mail\Plugin\EmailBuilder;

use Drupal\advancedqueue\Job;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailBuilderBase;
use Drupal\symfony_mailer\Processor\TokenProcessorTrait;

/**
 * Defines the Email Builder plug-in for advancedqueue mails.
 *
 * @EmailBuilder(
 *   id = "advancedqueue_mail",
 *   label = "Advanced Queue mail",
 *   sub_types = {
 *     "on_success" = @Translation("Job success"),
 *     "on_retry" = @Translation("Job retry"),
 *     "on_failure" = @Translation("Job failure"),
 *   },
 * )
 */
class AdvancedQueueMail extends EmailBuilderBase {

  use TokenProcessorTrait;

  /**
   * Saves the parameters for a newly created email.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email to modify.
   * @param \Drupal\advancedqueue\Job|null $job
   *   The Job object.
   */
  public function createParams(EmailInterface $email, Job $job = NULL) {
    assert($job != NULL);
    $email->setParam('job', $job);
  }

}
