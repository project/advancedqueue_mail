<?php

namespace Drupal\advancedqueue_mail\EventSubscriber;

use Drupal\advancedqueue\Event\AdvancedQueueEvents;
use Drupal\advancedqueue\Event\JobEvent;
use Drupal\advancedqueue\Job;
use Drupal\symfony_mailer\EmailFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for advancedqueue events.
 */
class AdvancedQueueEventSubscriber implements EventSubscriberInterface {

  /**
   * The email factory service.
   *
   * @var \Drupal\symfony_mailer\EmailFactoryInterface
   */
  protected EmailFactoryInterface $emailFactory;

  /**
   * AdvancedQueueEventSubscriber constructor.
   *
   * @param \Drupal\symfony_mailer\EmailFactoryInterface $email_factory
   *   The email factory service.
   */
  public function __construct(EmailFactoryInterface $email_factory) {
    $this->emailFactory = $email_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[AdvancedQueueEvents::ON_SUCCESS][] = ['queueJobSuccess'];
    $events[AdvancedQueueEvents::ON_RETRY][] = ['queueJobRetry'];
    $events[AdvancedQueueEvents::ON_FAILURE][] = ['queueJobFailure'];
    return $events;
  }

  /**
   * Reacts on queue job success.
   *
   * @param \Drupal\advancedqueue\Event\JobEvent $event
   *   The triggered event.
   */
  public function queueJobSuccess(JobEvent $event): void {
    $this->sendNotificationMail($event->getJob(), 'on_success');
  }

  /**
   * Reacts on queue job retry.
   *
   * @param \Drupal\advancedqueue\Event\JobEvent $event
   *   The triggered event.
   */
  public function queueJobRetry(JobEvent $event): void {
    $this->sendNotificationMail($event->getJob(), 'on_retry');
  }

  /**
   * Reacts on queue job failure.
   *
   * @param \Drupal\advancedqueue\Event\JobEvent $event
   *   The triggered event.
   */
  public function queueJobFailure(JobEvent $event): void {
    $this->sendNotificationMail($event->getJob(), 'on_failure');
  }

  /**
   * Send a notification mail for advanced queue status updates.
   *
   * @param \Drupal\advancedqueue\Job $job
   *   The advancedqueue job object.
   * @param string $sub_type
   *   The Symfony Mailer subtype, based on triggered event.
   */
  protected function sendNotificationMail(Job $job, string $sub_type): void {
    $email = $this->emailFactory->newTypedEmail('advancedqueue_mail', $sub_type, $job);
    $email->setVariable('job', $job);
    $email->send();
  }

}
